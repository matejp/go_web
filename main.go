package main

import (
	"os"
	"fmt"
	"net/http"
	"html/template"
)

var (
	serverIp      = os.Getenv("IP")
	serverPort    = os.Getenv("PORT")
	addressString = serverIp + ":" + serverPort
)

func handler(w http.ResponseWriter, r *http.Request) {
	t  := template.Must(template.ParseFiles("static/index.html",
											"static/include-cdn.html"))
	// t.ExecuteTemplate(w, "static/index.html", nil)
	t.ExecuteTemplate(w, "include-cdn", nil)
    // t, _ := template.ParseFiles("static/index.html") //setp 1
    t.Execute(w, "Hello World!") //step 2

	// fmt.Fprintf(w, "Salut, j'aime %s!", r.URL.Path[1:])
}

func main() {
	if serverIp == "" {
		serverIp = "0.0.0.0"
	}
	if serverPort == "" {
		serverPort = "8080"
	}
	addressString = serverIp + ":" + serverPort

	fmt.Println("Server running on: " + addressString)
	http.HandleFunc("/", handler)
	http.ListenAndServe(addressString, nil)
}
